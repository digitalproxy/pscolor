(function($) {

	// Create the defaults once
	var pscolor = 'pscolor',
		defaults = {
			livePreview: true,
			shelfColor: '000000',
			color: 'ff3d3d',
			angle: 90,
			onChange: function() {},
			onSubmit: function() {},
			onClose: function() {},
			swatches: [
				"ff716c",
				"ff71c4",
				"d2c1c4",
				"52c1c4",
				"88dbde",
				"ffdbde",
				"ffdb3d",
				"ff3d3d",
				"ff3d34"
			],
			swatchSet: {
				"Web": ['000000', '000033', '000066', '000099', '0000CC', '0000FF', '003300', '003333', '003366', '003399', '0033CC', '0033FF', '006600', '006633', '006666', '006699', '0066CC', '0066FF', '009900', '009933', '009966', '009999', '0099CC', '0099FF', '00CC00', '00CC33', '00CC66', '00CC99', '00CCCC', '00CCFF', '00FF00', '00FF33', '00FF66', '00FF99', '00FFCC', '00FFFF', '330000', '330033', '330066', '330099', '3300CC', '3300FF', '333300', '333333', '333366', '333399', '3333CC', '3333FF', '336600', '336633', '336666', '336699', '3366CC', '3366FF', '339900', '339933', '339966', '339999', '3399CC', '3399FF', '33CC00', '33CC33', '33CC66', '33CC99', '33CCCC', '33CCFF', '33FF00', '33FF33', '33FF66', '33FF99', '33FFCC', '33FFFF', '660000', '660033', '660066', '660099', '6600CC', '6600FF', '663300', '663333', '663366', '663399', '6633CC', '6633FF', '666600', '666633', '666666', '666699', '6666CC', '6666FF', '669900', '669933', '669966', '669999', '6699CC', '6699FF', '66CC00', '66CC33', '66CC66', '66CC99', '66CCCC', '66CCFF', '66FF00', '66FF33', '66FF66', '66FF99', '66FFCC', '66FFFF', '990000', '990033', '990066', '990099', '9900CC', '9900FF', '993300', '993333', '993366', '993399', '9933CC', '9933FF', '996600', '996633', '996666', '996699', '9966CC', '9966FF', '999900', '999933', '999966', '999999', '9999CC', '9999FF', '99CC00', '99CC33', '99CC66', '99CC99', '99CCCC', '99CCFF', '99FF00', '99FF33', '99FF66', '99FF99', '99FFCC', '99FFFF', 'CC0000', 'CC0033', 'CC0066', 'CC0099', 'CC00CC', 'CC00FF', 'CC3300', 'CC3333', 'CC3366', 'CC3399', 'CC33CC', 'CC33FF', 'CC6600', 'CC6633', 'CC6666', 'CC6699', 'CC66CC', 'CC66FF', 'CC9900', 'CC9933', 'CC9966', 'CC9999', 'CC99CC', 'CC99FF', 'CCCC00', 'CCCC33', 'CCCC66', 'CCCC99', 'CCCCCC', 'CCCCFF', 'CCFF00', 'CCFF33', 'CCFF66', 'CCFF99', 'CCFFCC', 'CCFFFF', 'FF0000', 'FF0033', 'FF0066', 'FF0099', 'FF00CC', 'FF00FF', 'FF3300', 'FF3333', 'FF3366', 'FF3399', 'FF33CC', 'FF33FF', 'FF6600', 'FF6633', 'FF6666', 'FF6699', 'FF66CC', 'FF66FF', 'FF9900', 'FF9933', 'FF9966', 'FF9999', 'FF99CC', 'FF99FF', 'FFCC00', 'FFCC33', 'FFCC66', 'FFCC99', 'FFCCCC', 'FFCCFF', 'FFFF00', 'FFFF33', 'FFFF66', 'FFFF99', 'FFFFCC', 'FFFFFF'],
				"Red": ['FFFFFF', 'FFFFF9', 'FFFFEC', 'FFFFE8', 'FFFFDF', 'FFFFD2', 'FFFFC8', 'FFFFBF', 'FFFFFF', 'FFFFF3', 'FFFFDA', 'FFFFD2', 'FFFFBF', 'FFFFA6', 'FFFF91', 'FFFF7F', 'FFFFFF', 'FFFFED', 'FFFFC8', 'FFFFBB', 'FFFF9F', 'FFFF7A', 'FFFF5A', 'FFFF3F', 'FFFFFF', 'FFFFEA', 'FFFFBD', 'FFFFAE', 'FFFF8C', 'FFFF60', 'FFFF39', 'FFFF19', 'FFFFFF', 'FFFFE7', 'FFFFB6', 'FFFFA5', 'FFFF7F', 'FFFF4E', 'FFFF23', 'FFFF00', 'EEEEEE', 'EEEED8', 'EEEEAA', 'EEEE9A', 'EEEE77', 'EEEE49', 'EEEE21', 'EEEE00', 'CDCDCD', 'CDCDBA', 'CDCD92', 'CDCD85', 'CDCD66', 'CDCD3F', 'CDCD1C', 'CDCD00', '8B8B8B', '8B8B7E', '8B8B63', '8B8B5A', '8B8B45', '8B8B2A', '8B8B13', '8B8B00', 'FFF9FF', 'FFF9F9', 'FFF9EC', 'FFF9E8', 'FFF9DF', 'FFF9D2', 'FFF9C8', 'FFF9BF', 'FFF3FF', 'FFF3F3', 'FFF3DA', 'FFF3D2', 'FFF3BF', 'FFF3A6', 'FFF391', 'FFF37F', 'FFEDFF', 'FFEDED', 'FFEDC8', 'FFEDBB', 'FFED9F', 'FFED7A', 'FFED5A', 'FFED3F', 'FFEAFF', 'FFEAEA', 'FFEABD', 'FFEAAE', 'FFEA8C', 'FFEA60', 'FFEA39', 'FFEA19', 'FFE7FF', 'FFE7E7', 'FFE7B6', 'FFE7A5', 'FFE77F', 'FFE74E', 'FFE723', 'FFE700', 'EED8EE', 'EED8D8', 'EED8AA', 'EED89A', 'EED877', 'EED849', 'EED821', 'EED800', 'CDBACD', 'CDBABA', 'CDBA92', 'CDBA85', 'CDBA66', 'CDBA3F', 'CDBA1C', 'CDBA00', '8B7E8B', '8B7E7E', '8B7E63', '8B7E5A', '8B7E45', '8B7E2A', '8B7E13', '8B7E00', 'FFECFF', 'FFECF9', 'FFECEC', 'FFECE8', 'FFECDF', 'FFECD2', 'FFECC8', 'FFECBF', 'FFDAFF', 'FFDAF3', 'FFDADA', 'FFDAD2', 'FFDABF', 'FFDAA6', 'FFDA91', 'FFDA7F', 'FFC8FF', 'FFC8ED', 'FFC8C8', 'FFC8BB', 'FFC89F', 'FFC87A', 'FFC85A', 'FFC83F', 'FFBDFF', 'FFBDEA', 'FFBDBD', 'FFBDAE', 'FFBD8C', 'FFBD60', 'FFBD39', 'FFBD19', 'FFB6FF', 'FFB6E7', 'FFB6B6', 'FFB6A5', 'FFB67F', 'FFB64E', 'FFB623', 'FFB600', 'EEAAEE', 'EEAAD8', 'EEAAAA', 'EEAA9A', 'EEAA77', 'EEAA49', 'EEAA21', 'EEAA00', 'CD92CD', 'CD92BA', 'CD9292', 'CD9285', 'CD9266', 'CD923F', 'CD921C', 'CD9200', '8B638B', '8B637E', '8B6363', '8B635A', '8B6345', '8B632A', '8B6313', '8B6300', 'FFE8FF', 'FFE8F9', 'FFE8EC', 'FFE8E8', 'FFE8DF', 'FFE8D2', 'FFE8C8', 'FFE8BF', 'FFD2FF', 'FFD2F3', 'FFD2DA', 'FFD2D2', 'FFD2BF', 'FFD2A6', 'FFD291', 'FFD27F', 'FFBBFF', 'FFBBED', 'FFBBC8', 'FFBBBB', 'FFBB9F', 'FFBB7A', 'FFBB5A', 'FFBB3F', 'FFAEFF', 'FFAEEA', 'FFAEBD', 'FFAEAE', 'FFAE8C', 'FFAE60', 'FFAE39', 'FFAE19', 'FFA5FF', 'FFA5E7', 'FFA5B6', 'FFA5A5', 'FFA57F', 'FFA54E', 'FFA523', 'FFA500', 'EE9AEE', 'EE9AD8', 'EE9AAA', 'EE9A9A', 'EE9A77', 'EE9A49', 'EE9A21', 'EE9A00', 'CD85CD', 'CD85BA', 'CD8592', 'CD8585', 'CD8566', 'CD853F', 'CD851C', 'CD8500', '8B5A8B', '8B5A7E', '8B5A63', '8B5A5A', '8B5A45', '8B5A2A', '8B5A13', '8B5A00', 'FFDFFF', 'FFDFF9', 'FFDFEC', 'FFDFE8', 'FFDFDF', 'FFDFD2', 'FFDFC8', 'FFDFBF', 'FFBFFF', 'FFBFF3', 'FFBFDA', 'FFBFD2', 'FFBFBF', 'FFBFA6', 'FFBF91', 'FFBF7F', 'FF9FFF', 'FF9FED', 'FF9FC8', 'FF9FBB', 'FF9F9F', 'FF9F7A', 'FF9F5A', 'FF9F3F', 'FF8CFF', 'FF8CEA', 'FF8CBD', 'FF8CAE', 'FF8C8C', 'FF8C60', 'FF8C39', 'FF8C19', 'FF7FFF', 'FF7FE7', 'FF7FB6', 'FF7FA5', 'FF7F7F', 'FF7F4E', 'FF7F23', 'FF7F00', 'EE77EE', 'EE77D8', 'EE77AA', 'EE779A', 'EE7777', 'EE7749', 'EE7721', 'EE7700', 'CD66CD', 'CD66BA', 'CD6692', 'CD6685', 'CD6666', 'CD663F', 'CD661C', 'CD6600', '8B458B', '8B457E', '8B4563', '8B455A', '8B4545', '8B452A', '8B4513', '8B4500', 'FFD2FF', 'FFD2F9', 'FFD2EC', 'FFD2E8', 'FFD2DF', 'FFD2D2', 'FFD2C8', 'FFD2BF', 'FFA6FF', 'FFA6F3', 'FFA6DA', 'FFA6D2', 'FFA6BF', 'FFA6A6', 'FFA691', 'FFA67F', 'FF7AFF', 'FF7AED', 'FF7AC8', 'FF7ABB', 'FF7A9F', 'FF7A7A', 'FF7A5A', 'FF7A3F', 'FF60FF', 'FF60EA', 'FF60BD', 'FF60AE', 'FF608C', 'FF6060', 'FF6039', 'FF6019', 'FF4EFF', 'FF4EE7', 'FF4EB6', 'FF4EA5', 'FF4E7F', 'FF4E4E', 'FF4E23', 'FF4E00', 'EE49EE', 'EE49D8', 'EE49AA', 'EE499A', 'EE4977', 'EE4949', 'EE4921', 'EE4900', 'CD3FCD', 'CD3FBA', 'CD3F92', 'CD3F85', 'CD3F66', 'CD3F3F', 'CD3F1C', 'CD3F00', '8B2A8B', '8B2A7E', '8B2A63', '8B2A5A', '8B2A45', '8B2A2A', '8B2A13', '8B2A00', 'FFC8FF', 'FFC8F9', 'FFC8EC', 'FFC8E8', 'FFC8DF', 'FFC8D2', 'FFC8C8', 'FFC8BF', 'FF91FF', 'FF91F3', 'FF91DA', 'FF91D2', 'FF91BF', 'FF91A6', 'FF9191', 'FF917F', 'FF5AFF', 'FF5AED', 'FF5AC8', 'FF5ABB', 'FF5A9F', 'FF5A7A', 'FF5A5A', 'FF5A3F', 'FF39FF', 'FF39EA', 'FF39BD', 'FF39AE', 'FF398C', 'FF3960', 'FF3939', 'FF3919', 'FF23FF', 'FF23E7', 'FF23B6', 'FF23A5', 'FF237F', 'FF234E', 'FF2323', 'FF2300', 'EE21EE', 'EE21D8', 'EE21AA', 'EE219A', 'EE2177', 'EE2149', 'EE2121', 'EE2100', 'CD1CCD', 'CD1CBA', 'CD1C92', 'CD1C85', 'CD1C66', 'CD1C3F', 'CD1C1C', 'CD1C00', '8B138B', '8B137E', '8B1363', '8B135A', '8B1345', '8B132A', '8B1313', '8B1300', 'FFBFFF', 'FFBFF9', 'FFBFEC', 'FFBFE8', 'FFBFDF', 'FFBFD2', 'FFBFC8', 'FFBFBF', 'FF7FFF', 'FF7FF3', 'FF7FDA', 'FF7FD2', 'FF7FBF', 'FF7FA6', 'FF7F91', 'FF7F7F', 'FF3FFF', 'FF3FED', 'FF3FC8', 'FF3FBB', 'FF3F9F', 'FF3F7A', 'FF3F5A', 'FF3F3F', 'FF19FF', 'FF19EA', 'FF19BD', 'FF19AE', 'FF198C', 'FF1960', 'FF1939', 'FF1919', 'FF00FF', 'FF00E7', 'FF00B6', 'FF00A5', 'FF007F', 'FF004E', 'FF0023', 'FF0000', 'EE00EE', 'EE00D8', 'EE00AA', 'EE009A', 'EE0077', 'EE0049', 'EE0021', 'EE0000', 'CD00CD', 'CD00BA', 'CD0092', 'CD0085', 'CD0066', 'CD003F', 'CD001C', 'CD0000', '8B008B', '8B007E', '8B0063', '8B005A', '8B0045', '8B002A', '8B0013', '8B0000'],
				"Green": ['FFFFFF', 'FFFFF9', 'FFFFEC', 'FFFFE8', 'FFFFDF', 'FFFFD2', 'FFFFC8', 'FFFFBF', 'FFFFFF', 'FFFFF3', 'FFFFDA', 'FFFFD2', 'FFFFBF', 'FFFFA6', 'FFFF91', 'FFFF7F', 'FFFFFF', 'FFFFED', 'FFFFC8', 'FFFFBB', 'FFFF9F', 'FFFF7A', 'FFFF5A', 'FFFF3F', 'FFFFFF', 'FFFFEA', 'FFFFBD', 'FFFFAE', 'FFFF8C', 'FFFF60', 'FFFF39', 'FFFF19', 'FFFFFF', 'FFFFE7', 'FFFFB6', 'FFFFA5', 'FFFF7F', 'FFFF4E', 'FFFF23', 'FFFF00', 'EEEEEE', 'EEEED8', 'EEEEAA', 'EEEE9A', 'EEEE77', 'EEEE49', 'EEEE21', 'EEEE00', 'CDCDCD', 'CDCDBA', 'CDCD92', 'CDCD85', 'CDCD66', 'CDCD3F', 'CDCD1C', 'CDCD00', '8B8B8B', '8B8B7E', '8B8B63', '8B8B5A', '8B8B45', '8B8B2A', '8B8B13', '8B8B00', 'F9FFFF', 'F9FFF9', 'F9FFEC', 'F9FFE8', 'F9FFDF', 'F9FFD2', 'F9FFC8', 'F9FFBF', 'F3FFFF', 'F3FFF3', 'F3FFDA', 'F3FFD2', 'F3FFBF', 'F3FFA6', 'F3FF91', 'F3FF7F', 'EDFFFF', 'EDFFED', 'EDFFC8', 'EDFFBB', 'EDFF9F', 'EDFF7A', 'EDFF5A', 'EDFF3F', 'EAFFFF', 'EAFFEA', 'EAFFBD', 'EAFFAE', 'EAFF8C', 'EAFF60', 'EAFF39', 'EAFF19', 'E7FFFF', 'E7FFE7', 'E7FFB6', 'E7FFA5', 'E7FF7F', 'E7FF4E', 'E7FF23', 'E7FF00', 'D8EEEE', 'D8EED8', 'D8EEAA', 'D8EE9A', 'D8EE77', 'D8EE49', 'D8EE21', 'D8EE00', 'BACDCD', 'BACDBA', 'BACD92', 'BACD85', 'BACD66', 'BACD3F', 'BACD1C', 'BACD00', '7E8B8B', '7E8B7E', '7E8B63', '7E8B5A', '7E8B45', '7E8B2A', '7E8B13', '7E8B00', 'ECFFFF', 'ECFFF9', 'ECFFEC', 'ECFFE8', 'ECFFDF', 'ECFFD2', 'ECFFC8', 'ECFFBF', 'DAFFFF', 'DAFFF3', 'DAFFDA', 'DAFFD2', 'DAFFBF', 'DAFFA6', 'DAFF91', 'DAFF7F', 'C8FFFF', 'C8FFED', 'C8FFC8', 'C8FFBB', 'C8FF9F', 'C8FF7A', 'C8FF5A', 'C8FF3F', 'BDFFFF', 'BDFFEA', 'BDFFBD', 'BDFFAE', 'BDFF8C', 'BDFF60', 'BDFF39', 'BDFF19', 'B6FFFF', 'B6FFE7', 'B6FFB6', 'B6FFA5', 'B6FF7F', 'B6FF4E', 'B6FF23', 'B6FF00', 'AAEEEE', 'AAEED8', 'AAEEAA', 'AAEE9A', 'AAEE77', 'AAEE49', 'AAEE21', 'AAEE00', '92CDCD', '92CDBA', '92CD92', '92CD85', '92CD66', '92CD3F', '92CD1C', '92CD00', '638B8B', '638B7E', '638B63', '638B5A', '638B45', '638B2A', '638B13', '638B00', 'E8FFFF', 'E8FFF9', 'E8FFEC', 'E8FFE8', 'E8FFDF', 'E8FFD2', 'E8FFC8', 'E8FFBF', 'D2FFFF', 'D2FFF3', 'D2FFDA', 'D2FFD2', 'D2FFBF', 'D2FFA6', 'D2FF91', 'D2FF7F', 'BBFFFF', 'BBFFED', 'BBFFC8', 'BBFFBB', 'BBFF9F', 'BBFF7A', 'BBFF5A', 'BBFF3F', 'AEFFFF', 'AEFFEA', 'AEFFBD', 'AEFFAE', 'AEFF8C', 'AEFF60', 'AEFF39', 'AEFF19', 'A5FFFF', 'A5FFE7', 'A5FFB6', 'A5FFA5', 'A5FF7F', 'A5FF4E', 'A5FF23', 'A5FF00', '9AEEEE', '9AEED8', '9AEEAA', '9AEE9A', '9AEE77', '9AEE49', '9AEE21', '9AEE00', '85CDCD', '85CDBA', '85CD92', '85CD85', '85CD66', '85CD3F', '85CD1C', '85CD00', '5A8B8B', '5A8B7E', '5A8B63', '5A8B5A', '5A8B45', '5A8B2A', '5A8B13', '5A8B00', 'DFFFFF', 'DFFFF9', 'DFFFEC', 'DFFFE8', 'DFFFDF', 'DFFFD2', 'DFFFC8', 'DFFFBF', 'BFFFFF', 'BFFFF3', 'BFFFDA', 'BFFFD2', 'BFFFBF', 'BFFFA6', 'BFFF91', 'BFFF7F', '9FFFFF', '9FFFED', '9FFFC8', '9FFFBB', '9FFF9F', '9FFF7A', '9FFF5A', '9FFF3F', '8CFFFF', '8CFFEA', '8CFFBD', '8CFFAE', '8CFF8C', '8CFF60', '8CFF39', '8CFF19', '7FFFFF', '7FFFE7', '7FFFB6', '7FFFA5', '7FFF7F', '7FFF4E', '7FFF23', '7FFF00', '77EEEE', '77EED8', '77EEAA', '77EE9A', '77EE77', '77EE49', '77EE21', '77EE00', '66CDCD', '66CDBA', '66CD92', '66CD85', '66CD66', '66CD3F', '66CD1C', '66CD00', '458B8B', '458B7E', '458B63', '458B5A', '458B45', '458B2A', '458B13', '458B00', 'D2FFFF', 'D2FFF9', 'D2FFEC', 'D2FFE8', 'D2FFDF', 'D2FFD2', 'D2FFC8', 'D2FFBF', 'A6FFFF', 'A6FFF3', 'A6FFDA', 'A6FFD2', 'A6FFBF', 'A6FFA6', 'A6FF91', 'A6FF7F', '7AFFFF', '7AFFED', '7AFFC8', '7AFFBB', '7AFF9F', '7AFF7A', '7AFF5A', '7AFF3F', '60FFFF', '60FFEA', '60FFBD', '60FFAE', '60FF8C', '60FF60', '60FF39', '60FF19', '4EFFFF', '4EFFE7', '4EFFB6', '4EFFA5', '4EFF7F', '4EFF4E', '4EFF23', '4EFF00', '49EEEE', '49EED8', '49EEAA', '49EE9A', '49EE77', '49EE49', '49EE21', '49EE00', '3FCDCD', '3FCDBA', '3FCD92', '3FCD85', '3FCD66', '3FCD3F', '3FCD1C', '3FCD00', '2A8B8B', '2A8B7E', '2A8B63', '2A8B5A', '2A8B45', '2A8B2A', '2A8B13', '2A8B00', 'C8FFFF', 'C8FFF9', 'C8FFEC', 'C8FFE8', 'C8FFDF', 'C8FFD2', 'C8FFC8', 'C8FFBF', '91FFFF', '91FFF3', '91FFDA', '91FFD2', '91FFBF', '91FFA6', '91FF91', '91FF7F', '5AFFFF', '5AFFED', '5AFFC8', '5AFFBB', '5AFF9F', '5AFF7A', '5AFF5A', '5AFF3F', '39FFFF', '39FFEA', '39FFBD', '39FFAE', '39FF8C', '39FF60', '39FF39', '39FF19', '23FFFF', '23FFE7', '23FFB6', '23FFA5', '23FF7F', '23FF4E', '23FF23', '23FF00', '21EEEE', '21EED8', '21EEAA', '21EE9A', '21EE77', '21EE49', '21EE21', '21EE00', '1CCDCD', '1CCDBA', '1CCD92', '1CCD85', '1CCD66', '1CCD3F', '1CCD1C', '1CCD00', '138B8B', '138B7E', '138B63', '138B5A', '138B45', '138B2A', '138B13', '138B00', 'BFFFFF', 'BFFFF9', 'BFFFEC', 'BFFFE8', 'BFFFDF', 'BFFFD2', 'BFFFC8', 'BFFFBF', '7FFFFF', '7FFFF3', '7FFFDA', '7FFFD2', '7FFFBF', '7FFFA6', '7FFF91', '7FFF7F', '3FFFFF', '3FFFED', '3FFFC8', '3FFFBB', '3FFF9F', '3FFF7A', '3FFF5A', '3FFF3F', '19FFFF', '19FFEA', '19FFBD', '19FFAE', '19FF8C', '19FF60', '19FF39', '19FF19', '00FFFF', '00FFE7', '00FFB6', '00FFA5', '00FF7F', '00FF4E', '00FF23', '00FF00', '00EEEE', '00EED8', '00EEAA', '00EE9A', '00EE77', '00EE49', '00EE21', '00EE00', '00CDCD', '00CDBA', '00CD92', '00CD85', '00CD66', '00CD3F', '00CD1C', '00CD00', '008B8B', '008B7E', '008B63', '008B5A', '008B45', '008B2A', '008B13', '008B00'],
				"Blue": ['FFFFFF', 'F9FFFF', 'ECFFFF', 'E8FFFF', 'DFFFFF', 'D2FFFF', 'C8FFFF', 'BFFFFF', 'FFFFFF', 'F3FFFF', 'DAFFFF', 'D2FFFF', 'BFFFFF', 'A6FFFF', '91FFFF', '7FFFFF', 'FFFFFF', 'EDFFFF', 'C8FFFF', 'BBFFFF', '9FFFFF', '7AFFFF', '5AFFFF', '3FFFFF', 'FFFFFF', 'EAFFFF', 'BDFFFF', 'AEFFFF', '8CFFFF', '60FFFF', '39FFFF', '19FFFF', 'FFFFFF', 'E7FFFF', 'B6FFFF', 'A5FFFF', '7FFFFF', '4EFFFF', '23FFFF', '00FFFF', 'EEEEEE', 'D8EEEE', 'AAEEEE', '9AEEEE', '77EEEE', '49EEEE', '21EEEE', '00EEEE', 'CDCDCD', 'BACDCD', '92CDCD', '85CDCD', '66CDCD', '3FCDCD', '1CCDCD', '00CDCD', '8B8B8B', '7E8B8B', '638B8B', '5A8B8B', '458B8B', '2A8B8B', '138B8B', '008B8B', 'FFF9FF', 'F9F9FF', 'ECF9FF', 'E8F9FF', 'DFF9FF', 'D2F9FF', 'C8F9FF', 'BFF9FF', 'FFF3FF', 'F3F3FF', 'DAF3FF', 'D2F3FF', 'BFF3FF', 'A6F3FF', '91F3FF', '7FF3FF', 'FFEDFF', 'EDEDFF', 'C8EDFF', 'BBEDFF', '9FEDFF', '7AEDFF', '5AEDFF', '3FEDFF', 'FFEAFF', 'EAEAFF', 'BDEAFF', 'AEEAFF', '8CEAFF', '60EAFF', '39EAFF', '19EAFF', 'FFE7FF', 'E7E7FF', 'B6E7FF', 'A5E7FF', '7FE7FF', '4EE7FF', '23E7FF', '00E7FF', 'EED8EE', 'D8D8EE', 'AAD8EE', '9AD8EE', '77D8EE', '49D8EE', '21D8EE', '00D8EE', 'CDBACD', 'BABACD', '92BACD', '85BACD', '66BACD', '3FBACD', '1CBACD', '00BACD', '8B7E8B', '7E7E8B', '637E8B', '5A7E8B', '457E8B', '2A7E8B', '137E8B', '007E8B', 'FFECFF', 'F9ECFF', 'ECECFF', 'E8ECFF', 'DFECFF', 'D2ECFF', 'C8ECFF', 'BFECFF', 'FFDAFF', 'F3DAFF', 'DADAFF', 'D2DAFF', 'BFDAFF', 'A6DAFF', '91DAFF', '7FDAFF', 'FFC8FF', 'EDC8FF', 'C8C8FF', 'BBC8FF', '9FC8FF', '7AC8FF', '5AC8FF', '3FC8FF', 'FFBDFF', 'EABDFF', 'BDBDFF', 'AEBDFF', '8CBDFF', '60BDFF', '39BDFF', '19BDFF', 'FFB6FF', 'E7B6FF', 'B6B6FF', 'A5B6FF', '7FB6FF', '4EB6FF', '23B6FF', '00B6FF', 'EEAAEE', 'D8AAEE', 'AAAAEE', '9AAAEE', '77AAEE', '49AAEE', '21AAEE', '00AAEE', 'CD92CD', 'BA92CD', '9292CD', '8592CD', '6692CD', '3F92CD', '1C92CD', '0092CD', '8B638B', '7E638B', '63638B', '5A638B', '45638B', '2A638B', '13638B', '00638B', 'FFE8FF', 'F9E8FF', 'ECE8FF', 'E8E8FF', 'DFE8FF', 'D2E8FF', 'C8E8FF', 'BFE8FF', 'FFD2FF', 'F3D2FF', 'DAD2FF', 'D2D2FF', 'BFD2FF', 'A6D2FF', '91D2FF', '7FD2FF', 'FFBBFF', 'EDBBFF', 'C8BBFF', 'BBBBFF', '9FBBFF', '7ABBFF', '5ABBFF', '3FBBFF', 'FFAEFF', 'EAAEFF', 'BDAEFF', 'AEAEFF', '8CAEFF', '60AEFF', '39AEFF', '19AEFF', 'FFA5FF', 'E7A5FF', 'B6A5FF', 'A5A5FF', '7FA5FF', '4EA5FF', '23A5FF', '00A5FF', 'EE9AEE', 'D89AEE', 'AA9AEE', '9A9AEE', '779AEE', '499AEE', '219AEE', '009AEE', 'CD85CD', 'BA85CD', '9285CD', '8585CD', '6685CD', '3F85CD', '1C85CD', '0085CD', '8B5A8B', '7E5A8B', '635A8B', '5A5A8B', '455A8B', '2A5A8B', '135A8B', '005A8B', 'FFDFFF', 'F9DFFF', 'ECDFFF', 'E8DFFF', 'DFDFFF', 'D2DFFF', 'C8DFFF', 'BFDFFF', 'FFBFFF', 'F3BFFF', 'DABFFF', 'D2BFFF', 'BFBFFF', 'A6BFFF', '91BFFF', '7FBFFF', 'FF9FFF', 'ED9FFF', 'C89FFF', 'BB9FFF', '9F9FFF', '7A9FFF', '5A9FFF', '3F9FFF', 'FF8CFF', 'EA8CFF', 'BD8CFF', 'AE8CFF', '8C8CFF', '608CFF', '398CFF', '198CFF', 'FF7FFF', 'E77FFF', 'B67FFF', 'A57FFF', '7F7FFF', '4E7FFF', '237FFF', '007FFF', 'EE77EE', 'D877EE', 'AA77EE', '9A77EE', '7777EE', '4977EE', '2177EE', '0077EE', 'CD66CD', 'BA66CD', '9266CD', '8566CD', '6666CD', '3F66CD', '1C66CD', '0066CD', '8B458B', '7E458B', '63458B', '5A458B', '45458B', '2A458B', '13458B', '00458B', 'FFD2FF', 'F9D2FF', 'ECD2FF', 'E8D2FF', 'DFD2FF', 'D2D2FF', 'C8D2FF', 'BFD2FF', 'FFA6FF', 'F3A6FF', 'DAA6FF', 'D2A6FF', 'BFA6FF', 'A6A6FF', '91A6FF', '7FA6FF', 'FF7AFF', 'ED7AFF', 'C87AFF', 'BB7AFF', '9F7AFF', '7A7AFF', '5A7AFF', '3F7AFF', 'FF60FF', 'EA60FF', 'BD60FF', 'AE60FF', '8C60FF', '6060FF', '3960FF', '1960FF', 'FF4EFF', 'E74EFF', 'B64EFF', 'A54EFF', '7F4EFF', '4E4EFF', '234EFF', '004EFF', 'EE49EE', 'D849EE', 'AA49EE', '9A49EE', '7749EE', '4949EE', '2149EE', '0049EE', 'CD3FCD', 'BA3FCD', '923FCD', '853FCD', '663FCD', '3F3FCD', '1C3FCD', '003FCD', '8B2A8B', '7E2A8B', '632A8B', '5A2A8B', '452A8B', '2A2A8B', '132A8B', '002A8B', 'FFC8FF', 'F9C8FF', 'ECC8FF', 'E8C8FF', 'DFC8FF', 'D2C8FF', 'C8C8FF', 'BFC8FF', 'FF91FF', 'F391FF', 'DA91FF', 'D291FF', 'BF91FF', 'A691FF', '9191FF', '7F91FF', 'FF5AFF', 'ED5AFF', 'C85AFF', 'BB5AFF', '9F5AFF', '7A5AFF', '5A5AFF', '3F5AFF', 'FF39FF', 'EA39FF', 'BD39FF', 'AE39FF', '8C39FF', '6039FF', '3939FF', '1939FF', 'FF23FF', 'E723FF', 'B623FF', 'A523FF', '7F23FF', '4E23FF', '2323FF', '0023FF', 'EE21EE', 'D821EE', 'AA21EE', '9A21EE', '7721EE', '4921EE', '2121EE', '0021EE', 'CD1CCD', 'BA1CCD', '921CCD', '851CCD', '661CCD', '3F1CCD', '1C1CCD', '001CCD', '8B138B', '7E138B', '63138B', '5A138B', '45138B', '2A138B', '13138B', '00138B', 'FFBFFF', 'F9BFFF', 'ECBFFF', 'E8BFFF', 'DFBFFF', 'D2BFFF', 'C8BFFF', 'BFBFFF', 'FF7FFF', 'F37FFF', 'DA7FFF', 'D27FFF', 'BF7FFF', 'A67FFF', '917FFF', '7F7FFF', 'FF3FFF', 'ED3FFF', 'C83FFF', 'BB3FFF', '9F3FFF', '7A3FFF', '5A3FFF', '3F3FFF', 'FF19FF', 'EA19FF', 'BD19FF', 'AE19FF', '8C19FF', '6019FF', '3919FF', '1919FF', 'FF00FF', 'E700FF', 'B600FF', 'A500FF', '7F00FF', '4E00FF', '2300FF', '0000FF', 'EE00EE', 'D800EE', 'AA00EE', '9A00EE', '7700EE', '4900EE', '2100EE', '0000EE', 'CD00CD', 'BA00CD', '9200CD', '8500CD', '6600CD', '3F00CD', '1C00CD', '0000CD', '8B008B', '7E008B', '63008B', '5A008B', '45008B', '2A008B', '13008B', '00008B'],
			}
		};

	// The actual plugin constructor
	function PsColor(element, options) {
	    // console.log(element, options);
		this.element = $(element);

		// jQuery has an extend method that merges the
		// contents of two or more objects, storing the 
		// result in the first object. The first object 
		// is generally empty because we don't want to alter 
		// the default options for future instances of the plugin
		this.options = $.extend({}, defaults, options);

		this._defaults = defaults;
		this._name = pscolor;

		this.init(this.options);
	}
	//  Menu system
	PsColor.prototype.menuClick = function(ev) {
		var click = $(ev.target).attr('class');
		dom.find('#sliders').hide();
		dom.find('#color-wheel').hide();
		dom.find('#swatches').hide();
		dom.find('#color-boxes').hide();
		dom.find('#color-space').parent().hide();
		$(ev.target).parent().find('li').removeClass('active');
		$(ev.target).addClass('active');
		if ($(ev.target).hasClass('sliders')) {
			dom.find('#color-boxes').css('display', 'inline-block');;
			dom.find('#color-space').parent().css('display', 'inline-block');
			dom.find('#swatches').show();
			dom.find('#swatches').html('').append(dom.data('pscolor').that.swatches(dom.data('pscolor').swatches));
		} else if ($(ev.target).hasClass('swatches')) {
			dom.find('#swatches').show();
			dom.find('#swatches').html('').append(dom.data('pscolor').that.swatchPanel(dom.data('pscolor').swatchSet));
			dom.find('#swatch-sets').trigger('click');
		} else if ($(ev.target).hasClass('close')) {
			dom.find(".sliders").trigger('click');
		    dom.data('pscolor').onClose({dom:dom});
		} else {
			dom.find('#color-space').parent().css('display', 'inline-block');
			dom.find('#color-boxes').show();
		}
		dom.find('#' + click).show();
		dom.find('#color-type').find('.lab').removeAttr('disabled');
		if (click == 'color-wheel') {
			dom.find('#color-type').find('.lab').attr('disabled', 'disabled');
			dom.data('pscolor').that.setSelector(dom.data('pscolor').color, dom);
		}
	};
	// Called when color mode is changed (dropdown)
	PsColor.prototype.ctype = function(e) {
		dom.find('#RGB').hide();
		dom.find('#HSB').hide();
		dom.find('#LAB').hide();
		dom.find('#' + $(this).find(":selected").text()).show();
	};
	// Called when the color is changed
	PsColor.prototype.change = function(ev) {
		if ($(this).attr('id') == 'color-space-hex') {
			dom.data('pscolor').color = col = hexToHsb(fixHex(this.value));
		} else if ($(this).parent().parent().parent().attr('id') == 'HSB') {
			dom.data('pscolor').color = col = fixHSB({
				h: parseInt(dom.data('pscolor').fields.eq(5).val(), 10),
				s: parseInt(dom.data('pscolor').fields.eq(6).val(), 10),
				b: parseInt(dom.data('pscolor').fields.eq(7).val(), 10)
			});
            // Update all sliders
            dom.data('pscolor').that.update(dom);

		} else if ($(this).parent().parent().parent().attr('id') == 'LAB') {
			dom.data('pscolor').color = col = rgbToHsb(
				labToRgb(
					fixLAB({
						l: dom.data('pscolor').fields.eq(8).val(),
						a: dom.data('pscolor').fields.eq(9).val(),
						b: dom.data('pscolor').fields.eq(10).val()
					})));
                    // Update all sliders
                    dom.data('pscolor').that.update(dom);
		} else {
            var trgb = {
            	    r: parseInt(dom.data('pscolor').fields.eq(2).val(), 10),
                    g: parseInt(dom.data('pscolor').fields.eq(3).val(), 10),
                    b: parseInt(dom.data('pscolor').fields.eq(4).val(), 10)
                }
            var rgb = {r:0,g:0,b:0};
            rgb.r = (Math.min(255, Math.max(0, trgb.r)) > 0) ? Math.min(255, Math.max(0, trgb.r)) : 0;
            rgb.g = (Math.min(255, Math.max(0, trgb.g)) > 0) ? Math.min(255, Math.max(0, trgb.g)) : 0;
            rgb.b = (Math.min(255, Math.max(0, trgb.b)) > 0) ? Math.min(255, Math.max(0, trgb.b)) : 0;

			dom.data('pscolor').color = col = rgbToHsb(fixRGB(rgb));
            // Update all sliders
            dom.data('pscolor').that.update(dom);
		}
		dom.data('pscolor').onChange.apply(dom.parent(), [col, hsbToHex(col), hsbToRgb(col), dom.data('pscolor').el, 0]);
	};
	// Styles //
	// Change style on blur and on focus of inputs
	PsColor.prototype.blur = function(ev) {
		$(this).removeClass('pscolor_focus');
	};
	// Focus
	PsColor.prototype.focus = function() {
		dom.data('pscolor').fields.removeClass('pscolor_focus');
		$(this).addClass('pscolor_focus');
	};
	//  Color Slider functions
	PsColor.prototype.stripScrub = function(ev) {
		ev.preventDefault(); // ? ev.preventDefault() : ev.returnValue = false;
		$(ev.target).on('mousemove touchmove', ev, dom.data('pscolor').that.scrub);
		$(ev.target).trigger('mousemove');
		$(document).on('mouseup touchend', function(event) {
			$(ev.target).off('mousemove touchmove');
		});

	};
	// Changes the input
	PsColor.prototype.scrub = function(ev) {
		var el = $(ev.target),
			input = dom.find('.' + el.attr('id')),
			x = ev.pageX - el.offset().left,
			ratio = x / el.width(),
			corrected = parseInt(ratio * (input.attr('max') - parseInt(input.attr('min')))) + parseInt(input.attr('min'));
		// console.log("Scrub: ", el, "Input: ", input, "X: ", x, "Ratio: ",ratio, "Corrected: ", corrected );
		input.val(Math.ceil(corrected)).trigger('change');
	};
	// Puts the color-strip icons in place
	PsColor.prototype.setStrips = function(col, dom) {
		$.each(dom.data('pscolor').fields, function(k, el) {
			var vel = $(el);
			vel.parent().parent().find('.pscolor_arrs').css('left', parseInt(((vel.val() - parseInt(vel.attr('min'))) / (vel.attr('max') - vel.attr('min'))) * 255));
		});
		var vel = $(dom.data('pscolor').fields.eq(5));
		$("#cw_hue").find('.pscolor_arrs').css('top', parseInt(((vel.val() - parseInt(vel.attr('min'))) / (vel.attr('max') - vel.attr('min'))) * 255));
	};
	//Hue slider functions
    PsColor.prototype.downHue = function (ev) {
        ev.preventDefault ? ev.preventDefault() : ev.returnValue = false;
            $(document).on('mouseup touchend',
                dom.data('pscolor').that.upHue);
            $(document).on('mousemove touchmove',
                dom.data('pscolor').that.moveHue);

        input.val(Math.ceil(corrected)).trigger('change');
        return false;
    };
    PsColor.prototype.moveHue = function (ev) {
		var el = $(ev.target),
			input = dom.find('.hue-grade'),
			y = ev.pageY - el.offset().top,
			ratio = y / el.height(),
			corrected = parseInt(ratio * (input.attr('max') - parseInt(input.attr('min')))) + parseInt(input.attr('min'));

        input.val(Math.ceil(corrected)).trigger('change');
        return false;
    };
    PsColor.prototype.upHue = function (ev) {
        dom.data('pscolor').that.update(dom);
        $(document).off('mouseup touchend',dom.data('pscolor').that.upHue);
        $(document).off('mousemove touchmove',dom.data('pscolor').that.moveHue);
        return false;
    };
	
	//Increment/decrement arrows functions // Depreciated
	PsColor.prototype.downIncrement = function(ev) {
		ev.preventDefault ? ev.preventDefault() : ev.returnValue = false;
		var field = $(this).parent().find('input').focus();
		var current = {
			el: $(this).parent().addClass('pscolor_slider'),
			max: this.parentNode.className.indexOf('_hsb_h') > 0 ? 360 : (this.parentNode.className.indexOf('_hsb') > 0 ? 100 : 255),
			y: ev.pageY,
			field: field,
			val: parseInt(field.val(), 10),
			preview: $(this).parent().parent().data('pscolor').livePreview
		};
		$(document).mouseup(current, this.upIncrement);
		$(document).mousemove(current, this.moveIncrement);
	};
	PsColor.prototype.moveIncrement = function(ev) {
		ev.data.field.val(Math.max(0, Math.min(ev.data.max, parseInt(ev.data.val - ev.pageY + ev.data.y, 10))));
		if (ev.data.preview) {
			change.apply(ev.data.field.get(0), [true]);
		}
		return false;
	};
	PsColor.prototype.upIncrement = function(ev) {
		dom.data('pscolor').that.change.apply(ev.data.field.get(0), [true]);
		ev.data.el.removeClass('pscolor_slider').find('input').focus();
		$(document).off('mouseup', dom.data('pscolor').that.upIncrement);
		$(document).off('mousemove', dom.data('pscolor').that.moveIncrement);
		return false;
	};

	//  Color wheel selector
	PsColor.prototype.downSelector = function(ev) {
		ev.preventDefault ? ev.preventDefault() : ev.returnValue = false;
		var current = {
			that: this,
			pos: $(this).offset()
		};
		current.preview = dom.data('pscolor').livePreview;

		$(document).on('mouseup touchend', current, dom.data('pscolor').that.upSelector);
		$(document).on('mousemove touchmove', current, dom.data('pscolor').that.moveSelector);

		var pageX, pageY;
		if (ev.type == 'touchstart') {
			pageX = ev.originalEvent.changedTouches[0].pageX;
			pageY = ev.originalEvent.changedTouches[0].pageY;
		} else {
			pageX = ev.pageX;
			pageY = ev.pageY;
		}

		dom.data('pscolor').that.change.apply(
			dom.data('pscolor').fields
			.eq(7).val(parseInt(100 * (dom.data('pscolor').height - (pageY - current.pos.top)) / dom.data('pscolor').height, 10)).end()
			.eq(6).val(parseInt(100 * (pageX - current.pos.left) / dom.data('pscolor').height, 10))
			.get(0), [current.preview]
		);
		return false;
	};
	// Identify the color on the wheel
	PsColor.prototype.moveSelector = function(ev) {
		var pageX, pageY;
		var el = $(ev.target),
			x = ev.pageX - el.offset().left,
			y = ev.pageY - el.offset().top,
			ratiox = x / el.width(),
			ratioy = (el.height() - y) / el.height();

		if (ev.type == 'touchmove') {
			pageX = ev.originalEvent.changedTouches[0].pageX;
			pageY = ev.originalEvent.changedTouches[0].pageY;
		} else {
			pageX = ev.pageX;
			pageY = ev.pageY;
		}

		dom.data('pscolor').that.change.apply(
			dom.data('pscolor').fields
			.eq(7).val(parseInt(100 * (ratioy))).end()
			.eq(6).val(parseInt(100 * (ratiox)))
			.get(0), [ev.data.preview]
		);
		return false;
	};
	// Modify the color fields and remove event handler
	PsColor.prototype.upSelector = function(ev) {
		dom.data('pscolor').that.fillRGBFields(dom.data('pscolor').color, dom.get(0));
		dom.data('pscolor').that.fillHexFields(dom.data('pscolor').color, dom.get(0));
		$(document).off('mouseup touchend', dom.data('pscolor').that.upSelector);
		$(document).off('mousemove touchmove', dom.data('pscolor').that.moveSelector);
		return false;
	};
	//Set the round selector position
	PsColor.prototype.setSelector = function(hsb, dom) {
		dom.data('pscolor').selector.css('backgroundColor', '#' + hsbToHex({
			h: hsb.h,
			s: 100,
			b: 100
		}));
		var x = dom.find('div.pscolor_color').offset().left,
			y = dom.find('div.pscolor_color').offset().top + dom.find('div.pscolor_color').height();

		dom.data('pscolor').selectorIndic.css({
			left: parseInt(x + ((hsb.s * 255) / 100), 10),
			top: parseInt(y + (-(hsb.b * 255) / 100), 10)
		});
	};


	// Fill the inputs of the plugin
	// Update all fields
	PsColor.prototype.update = function(dom) {
		var col = dom.data('pscolor').color;
		dom.data('pscolor').that.fillRGBFields(col, dom.get(0));
		dom.data('pscolor').that.fillHexFields(col, dom.get(0));
		dom.data('pscolor').that.fillHSBFields(col, dom.get(0));
		dom.data('pscolor').that.fillLabFields(col, dom.get(0));
		dom.data('pscolor').that.setSelector(col, dom);
		dom.data('pscolor').that.setHue(col, dom.get(0));
		dom.data('pscolor').that.setNewColor(col, dom.get(0));
		dom.data('pscolor').that.setStrips(col, dom);

		dom.find('.red-grade.cw-i').val(dom.data('pscolor').fields.eq(2).val());
		dom.find('.green-grade.cw-i').val(dom.data('pscolor').fields.eq(3).val());
		dom.find('.blue-grade.cw-i').val(dom.data('pscolor').fields.eq(4).val());
		dom.find('.hue-grade.cw-i').val(dom.data('pscolor').fields.eq(5).val());
		dom.find('.sat-grade.cw-i').val(dom.data('pscolor').fields.eq(6).val());
		dom.find('.bri-grade.cw-i').val(dom.data('pscolor').fields.eq(7).val());
		dom.find('.opacity').val(dom.data('pscolor').fields.eq(1).val());

		if(typeof dom.data('pscolor').callback == 'function') {
            dom.data('pscolor').callback("#"+hsbToHex(col));
        }
        dom.data('pscolor').onChange({hex: "#" + hsbToHex(col), dom: dom});

	};
	// Callback
	PsColor.prototype.onColorChange = function(callback) {
	    if(typeof callback == 'function') {
	        dom.data('pscolor').callback = callback;
	    }
	};

	// RGB
	PsColor.prototype.fillRGBFields = function(hsb, dom) {
		var rgb = hsbToRgb(hsb);
		$(dom).data('pscolor').fields
			.eq(2).val(rgb.r).end()
			.eq(3).val(rgb.g).end()
			.eq(4).val(rgb.b).end();
	};
	// LAB
	PsColor.prototype.fillLabFields = function(hsb, dom) {
		var lab = rgbToLab(hsbToRgb(hsb));
		$(dom).data('pscolor').fields
			.eq(8).val(lab.l).end()
			.eq(9).val(lab.a).end()
			.eq(10).val(lab.b).end();
	};
	// HSB
	PsColor.prototype.fillHSBFields = function(hsb, dom) {
		var thsb = {
			h: hsb.h,
			s: 100,
			b: 100
		},
			rgb = hsbToRgb(thsb);
		$(dom).data('pscolor').fields
			.eq(5).val(Math.round(hsb.h)).end()
			.eq(6).val(Math.round(hsb.s)).parent().parent()
			.find('.strip').css({
				'background': 'linear-gradient(to right, rgb(255,255,255) 0%, rgb(' + rgb.r + ',' + rgb.g + ',' + rgb.b + '))'
			})
			.css({
				'background': '-ms-linear-gradient(left, rgb(255,255,255) 0%, rgb(' + rgb.r + ',' + rgb.g + ',' + rgb.b + '))'
			})
			.css({
				'background': '-o-linear-gradient(left, rgb(255,255,255) 0%, rgb(' + rgb.r + ',' + rgb.g + ',' + rgb.b + '))'
			})
			.css({
				'background': '-webkit-linear-gradient(left, rgb(255,255,255) 0%, rgb(' + rgb.r + ',' + rgb.g + ',' + rgb.b + '))'
			})
			.css({
				'background': '-moz-linear-gradient(left, rgb(255,255,255) 0%, rgb(' + rgb.r + ',' + rgb.g + ',' + rgb.b + '))'
			})
			.css({
				'background': '-webkit-gradient(linear,left top, right top, color-stop( 0%, rgb(255,255,255)), color-stop(100%, rgb(' + rgb.r + ',' + rgb.g + ',' + rgb.b + ')))'
			}).end().end().end().end()
			.eq(7).val(Math.round(hsb.b)).parent().parent()
			.find('.strip').css({
				'background': 'linear-gradient(to right, rgb(0,0,0) 0%, rgb(' + rgb.r + ',' + rgb.g + ',' + rgb.b + '))'
			})
			.css({
				'background': '-ms-linear-gradient(left, rgb(0,0,0) 0%, rgb(' + rgb.r + ',' + rgb.g + ',' + rgb.b + '))'
			})
			.css({
				'background': '-o-linear-gradient(left, rgb(0,0,0) 0%, rgb(' + rgb.r + ',' + rgb.g + ',' + rgb.b + '))'
			})
			.css({
				'background': '-webkit-linear-gradient(left, rgb(0,0,0) 0%, rgb(' + rgb.r + ',' + rgb.g + ',' + rgb.b + '))'
			})
			.css({
				'background': '-moz-linear-gradient(left, rgb(0,0,0) 0%, rgb(' + rgb.r + ',' + rgb.g + ',' + rgb.b + '))'
			})
			.css({
				'background': '-webkit-gradient(linear,left top, right top, color-stop( 0%, rgb(0,0,0)), color-stop(100%, rgb(' + rgb.r + ',' + rgb.g + ',' + rgb.b + ')))',
			}).end();
	};
	// Hex
	PsColor.prototype.fillHexFields = function(hsb, dom) {
		$(dom).data('pscolor').fields.eq(0).val("#" + hsbToHex(hsb));
	};

	//Set the hue selector position // Depreciated
	PsColor.prototype.setHue = function(hsb, dom) {
		$(dom).data('pscolor').hue.css('top', parseInt($(dom).data('pscolor').height - $(dom).data('pscolor').height * hsb.h / 360, 10));
	};

	//Set current and new colors
	PsColor.prototype.setBackColor = function(hsb, dom) {
		$(dom).data('pscolor').backColor.css('backgroundColor', '#' + hsbToHex(hsb));
	};
	PsColor.prototype.setNewColor = function(hsb, dom) {
		$(dom).data('pscolor').newColor.css('backgroundColor', '#' + hsbToHex(hsb));
	};

	// Swap back adn front colors
	PsColor.prototype.swapColors = function() {
		var col = dom.data('pscolor').shelfColor;
		var shelfCol = dom.data('pscolor').shelfColor = dom.data('pscolor').color;
		dom.data('pscolor').color = col;
		dom.data('pscolor').that.setBackColor(shelfCol, dom.get(0));
		dom.data('pscolor').that.update(dom);
	};

	//Submit button
	PsColor.prototype.clickSubmit = function(ev) {
		var col = dom.data('pscolor').color;
		//dom.data('pscolor').origColor = col;
		//this.setNewColor(col, dom.get(0));
		dom.data('pscolor').onSubmit({
		    hex: hsbToHex(col),
            rgb: hsbToRgb(col),
            hsb: col,
            customSwatches: dom.data('pscolor').swatches,
            gradient: dom.data('pscolor').gradients,
            dom: dom
        });
	};

	// Swatches
	PsColor.prototype.swatches = function(colors) {
		var template = $('<div/>', {
			class: 'swatch'
		}),
			block = "<div class='swatch add-color'><g class='i-plus' /></div>";

		$.each(colors, function(k, c) {
			block = block + template.clone().css('background', "#" + c).wrap('<p>').parent().html();
		});
		if (colors.length < 88) {
			for (i = 0; i < (88 - colors.length); i++) {
				block = block + template.clone().wrap('<p>').parent().html();
			}
		}
		block = block + "<div class='swatch cycle'><g class='i-cycle' /></div>";
		return block;
	};
	// Swatch panel (from menu)
	PsColor.prototype.swatchPanel = function(swatches) {
		var template = $('<div/>', {
			class: 'swatch'
		}),
			list = [],
			panel = '',
			sets = '';
		$.each(swatches, function(k, i) {
			list.unshift(k);
			var colors = i,
				set = '',
				block = '<div class="' + k + ' swatch-sets" style="display: none;">';
			$.each(colors, function(k, c) {
				set = template.clone().css('background', "#" + c).wrap('<p>').parent().html() + set;
			});
			block = block + set + "</div>";
			panel = panel + block + "</div>";
		});
		$.each(list, function(k, i) {
			sets = "<option>" + i + "</option>" + sets;
		});
		panel = "<div class='row' style='height: 30px;'><div style='float:right;'><select id='swatch-sets' name='swatch-set'>" + sets + "</select></div></div>" + panel;
		return panel;

	};
	// Swatch sub-panel
	PsColor.prototype.swatchSetSwap = function(ev) {
		//		dom.find('#' + $(this).find(":selected").text()).show();
		dom.find('.swatch-sets').hide();
		dom.find("." + $(ev.target).find(':selected').text()).show();
	},

	// Add color to custom swatch panel (from Sliders)
	PsColor.prototype.swatchAdd = function(ev) {
		var swatches = dom.data('pscolor').swatches;
		swatches.unshift(hsbToHex(dom.data('pscolor').color));
		dom.find('#swatches').html('').append(dom.data('pscolor').that.swatches(dom.data('pscolor').swatches));
	};
	// Apply the swatched color to foreground
	PsColor.prototype.swatchApply = function(ev) {
		var color = $(ev.target).css('backgroundColor');
		var col = rgbToHsb(cleanRgb(color));


		dom.data('pscolor').color = col;
		dom.data('pscolor').that.update(dom);

	};

	// Gradients
	PsColor.prototype.gradientClick = function(ev) {
		dom.find('#gradient-container').show();
	    dom.data('pscolor').gradient.find('.bumper').parent().addClass('top-border');

		dom.data('pscolor').gradientObj = new Gradient(dom.find("#gradient-container"),{
			dom: dom,
			items: dom.find("#gradient-items")
		});
		dom.data('pscolor').gradient.find('.bumper').off('click');
	};

	// Plugin init
	PsColor.prototype.init = function(opt) {
		opt = $.extend({}, defaults, opt || {});
		if (typeof opt.color == 'string') {
			opt.color = hexToHsb(opt.color);
		} else if (opt.color.r !== undefined && opt.color.g !== undefined && opt.color.b !== undefined) {
			opt.color = rgbToHsb(opt.color);
		} else if (opt.color.h !== undefined && opt.color.s !== undefined && opt.color.b !== undefined) {
			opt.color = fixHSB(opt.color);
		} else {
			return this;
		}

		var options = $.extend({}, opt);
		dom = this.element;
        // console.log(options);
		// Store parts of the plugin
		options.el = this; // Store element
		options.origColor = opt.color;
		options.hue = dom.find('div#cw_hue'); // Store gradient object for color wheel
		options.newColor = dom.find('div.front'); // Store front color object
		options.backColor = dom.find('div.back').click(this.swapColors); // Store back color object
		options.gradient = dom.find('#gradient');
		options.that = this;
		options.gradients = {};
		// Setup input fields
		options.fields = dom.find('input:not(.cw-i)')
			.change(this, this.change)
			.blur(this, this.blur)
			.focus(this, this.focus);
		options.strips = dom.find('.strip')
			.on('mousedown', this.stripScrub).end();
		// Setup color model
		options.type = dom.find('select')
			.change(this, this.ctype);
		// Setup hue selector
		options.hue.on('mousedown touchstart', this.downHue);
		options.selector = dom.find('div.pscolor_color')
			.on('mousedown touchstart', this.downSelector);
		options.selectorIndic = options.selector.find('div.pscolor_selector_outer');

		// Generate the first swatch panel
		dom.find('#swatches').append(this.swatches(options.swatches));

		// Generate and assign a random ID
		var id = 'pscolor_' + parseInt(Math.random() * 1000);
		$(this).data('pscolorClass', id);
		//Set the class and get the HTML
		this.element.attr('class', id);

		// Setup submit button
		dom.find('input[type="submit"]').click(this, this.clickSubmit);
		// Setup menu bar
		dom.find('ul li').click(this, this.menuClick);

		// Watch swatch
		dom.delegate('.add-color', 'click', this.swatchAdd);
		dom.delegate('.cycle', 'click', this.swatchAdd);
		dom.delegate('#swatch-sets', 'click', this.swatchSetSwap);
		dom.delegate('.swatch:not(.add-color)', 'click', this.swatchApply);

		// Watch other elements
		options.gradient.find('.bumper').on('click', this.gradientClick);

		//Store options and fill with default color
		dom.data('pscolor', options);

		// Trickle down values
		this.update(dom);
		dom.find('.swatches').trigger('click');
		dom.find('.sliders').trigger('click');

	};

	// Color fixes
	var fixHSB = function(hsb) {
		return {
			h: Math.min(360, Math.max(0, hsb.h)),
			s: Math.min(100, Math.max(0, hsb.s)),
			b: Math.min(100, Math.max(0, hsb.b))
		};
	};
	var fixRGB = function(rgb) {
		return {
			r: Math.min(255, Math.max(0, rgb.r)),
			g: Math.min(255, Math.max(0, rgb.g)),
			b: Math.min(255, Math.max(0, rgb.b))
		};
	};
	var fixLAB = function(lab) {
		return {
			l: Math.min(128, Math.max(-128, lab.l)),
			a: Math.min(128, Math.max(-128, lab.a)),
			b: Math.min(128, Math.max(-128, lab.b))
		};
	};
	var fixHex = function(hex) {
		var len = 6 - hex.length;
		if (len > 0) {
			var o = [];
			for (var i = 0; i < len; i++) {
				o.push('0');
			}
			o.push(hex);
			hex = o.join('');
		}
		return hex;
	};

    var cleanRgb = function(color) {
        var rgb = color.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
        var vrgb = {};
        vrgb.r = parseInt(rgb[1],10);
        vrgb.g = parseInt(rgb[2],10);
        vrgb.b = parseInt(rgb[3],10);
        return vrgb;
    };
	//Color space conversions
	// Hex ->
	var hexToRgb = function(hex) {
		var hex = parseInt(((hex.indexOf('#') > -1) ? hex.substring(1) : hex), 16);
		return {
			r: hex >> 16,
			g: (hex & 0x00FF00) >> 8,
			b: (hex & 0x0000FF)
		};
	};
	var hexToHsb = function(hex) {
		return rgbToHsb(hexToRgb(hex));
	};
	var hsbToHex = function(hsb) {
		return rgbToHex(hsbToRgb(hsb));
	};
	// RGB ->
	var rgbToHsb = function(rgb) {
		var hsb = {
			h: 0,
			s: 0,
			b: 0
		};

		var min = Math.min(rgb.r, rgb.g, rgb.b);
		var max = Math.max(rgb.r, rgb.g, rgb.b);
		var delta = max - min;
		hsb.b = max;
		hsb.s = max !== 0 ? 255 * delta / max : 0;
		if (hsb.s !== 0) {
			if (rgb.r == max) hsb.h = (rgb.g - rgb.b) / delta;
			else if (rgb.g == max) hsb.h = 2 + (rgb.b - rgb.r) / delta;
			else hsb.h = 4 + (rgb.r - rgb.g) / delta;
		} else hsb.h = -1;
		hsb.h *= 60;
		if (hsb.h < 0) hsb.h += 360;
            hsb.s *= 100 / 255;
            hsb.b *= 100 / 255;
		return hsb;
	};
	var rgbToHex = function(rgb) {
		var hex = [
			rgb.r.toString(16),
			rgb.g.toString(16),
			rgb.b.toString(16)
		];
		$.each(hex, function(nr, val) {
			if (val.length == 1) {
				hex[nr] = '0' + val;
			}
		});
		return hex.join('');
	};
	var rgbToXyz = function(rgb) {
		var r = parseFloat(rgb.r / 255),
			g = parseFloat(rgb.g / 255),
			b = parseFloat(rgb.b / 255);

		// assume sRGB
		r = r > 0.04045 ? Math.pow(((r + 0.055) / 1.055), 2.4) : (r / 12.92);
		g = g > 0.04045 ? Math.pow(((g + 0.055) / 1.055), 2.4) : (g / 12.92);
		b = b > 0.04045 ? Math.pow(((b + 0.055) / 1.055), 2.4) : (b / 12.92);

		r = r * 100;
		g = g * 100;
		b = b * 100;

		var x = (r * 0.4124) + (g * 0.3576) + (b * 0.1805);
		var y = (r * 0.2126) + (g * 0.7152) + (b * 0.0722);
		var z = (r * 0.0193) + (g * 0.1192) + (b * 0.9505);

		return {
			x: x,
			y: y,
			z: z
		};
	};

	var rgbToLab = function(rgb) {
		var lab = {};
		var xyz = rgbToXyz(rgb),
			x = xyz.x,
			y = xyz.y,
			z = xyz.z,
			l, a, b;

		x /= 95.047;
		y /= 100;
		z /= 108.883;

		x = x > 0.008856 ? Math.pow(x, 1 / 3) : (7.787 * x) + (16 / 116);
		y = y > 0.008856 ? Math.pow(y, 1 / 3) : (7.787 * y) + (16 / 116);
		z = z > 0.008856 ? Math.pow(z, 1 / 3) : (7.787 * z) + (16 / 116);

		l = (116 * y) - 16;
		a = 500 * (x - y);
		b = 200 * (y - z);
		return {
			l: l,
			a: a,
			b: b
		};
	};
	// HSB ->
	var hsbToRgb = function(hsb) {
		var rgb = {};
		var h = hsb.h;
		var s = hsb.s * 255 / 100;
		var v = hsb.b * 255 / 100;
		if (s === 0) {
			rgb.r = rgb.g = rgb.b = v;
		} else {
			var t1 = v;
			var t2 = (255 - s) * v / 255;
			var t3 = (t1 - t2) * (h % 60) / 60;
			if (h == 360) h = 0;
			if (h < 60) {
				rgb.r = t1;
				rgb.b = t2;
				rgb.g = t2 + t3;
			} else if (h < 120) {
				rgb.g = t1;
				rgb.b = t2;
				rgb.r = t1 - t3;
			} else if (h < 180) {
				rgb.g = t1;
				rgb.r = t2;
				rgb.b = t2 + t3;
			} else if (h < 240) {
				rgb.b = t1;
				rgb.r = t2;
				rgb.g = t1 - t3;
			} else if (h < 300) {
				rgb.b = t1;
				rgb.g = t2;
				rgb.r = t2 + t3;
			} else if (h < 360) {
				rgb.r = t1;
				rgb.g = t2;
				rgb.b = t1 - t3;
			} else {
				rgb.r = 0;
				rgb.g = 0;
				rgb.b = 0;
			}
		}
		return {
			r: Math.round(rgb.r),
			g: Math.round(rgb.g),
			b: Math.round(rgb.b)
		};
	};
	// LAB ->
	var labToRgb = function(lab) {
		rgb = xyzToRgb(labToXyz(lab));
		return {
			r: Math.round(rgb.r),
			g: Math.round(rgb.g),
			b: Math.round(rgb.b)
		};
	};
	var labToXyz = function(lab) {
		var l = lab.l,
			a = lab.a,
			b = lab.b,
			x, y, z, y2;
		y = (l + 16) / 116;
		x = (a / 500) + y;
		z = y - b / 200;

		y = Math.pow(y, 3) > 0.008856 ? Math.pow(y, 3) : (y - 16 / 116) / 7.787;
		x = Math.pow(x, 3) > 0.008856 ? Math.pow(x, 3) : (x - 16 / 116) / 7.787;
		z = Math.pow(z, 3) > 0.008856 ? Math.pow(z, 3) : (z - 16 / 116) / 7.787;

		x = x * 95.047;
		y = y * 100.000;
		z = z * 108.883;
		return {
			x: x,
			y: y,
			z: z
		};
	};
	var xyzToRgb = function(xyz) {
		var x = xyz.x / 100,
			y = xyz.y / 100,
			z = xyz.z / 100,
			r, g, b;

		r = (x * 3.2406) + (y * -1.5372) + (z * -0.4986);
		g = (x * -0.9689) + (y * 1.8758) + (z * 0.0415);
		b = (x * 0.0557) + (y * -0.2040) + (z * 1.0570);

		// assume sRGB
		r = r > 0.0031308 ? ((1.055 * Math.pow(r, 1.0 / 2.4)) - 0.055) : r = (r * 12.92);

		g = g > 0.0031308 ? ((1.055 * Math.pow(g, 1.0 / 2.4)) - 0.055) : g = (g * 12.92);

		b = b > 0.0031308 ? ((1.055 * Math.pow(b, 1.0 / 2.4)) - 0.055) : b = (b * 12.92);

		r = Math.min(Math.max(0, r), 1);
		g = Math.min(Math.max(0, g), 1);
		b = Math.min(Math.max(0, b), 1);
		return {
			r: r * 255,
			g: g * 255,
			b: b * 255
		};
	};
		
		
	function Gradient(element, options) {
		this.element = $(element);

		// jQuery has an extend method that merges the
		// contents of two or more objects, storing the 
		// result in the first object. The first object 
		// is generally empty because we don't want to alter 
		// the default options for future instances of the plugin
		this.options = options;

		$.extend({}, defaults, options);

		this._defaults = defaults;
		this._name = 'gradient';

		this.init();
	}

	Gradient.prototype.init = function() {
        var opt = $.extend({}, defaults, opt || {});
		var options = $.extend({}, opt);
        $.extend(this, this.options)
		var items = $(this.items);
		items.append(this.gradientDiv());
        var that = this;
		$(".cs_knob").knob({
            min : 0,
            max : 360,
            step : 1,
            angleOffset : 0,
            angleArc : 360,
            stopper : true,
            readOnly : false,
            cursor : 1,
            lineCap : 'none',
            thickness : '1',
            width : 60,
            height : 60,
            displayInput : false,
            displayPrevious : true,
            fgColor : '#ABB3BD',
            inputColor : '#ABB3BD',
            font : 'Arial',
            fontWeight : '0',
            bgColor : '#EBEEF2',
            change: function(v)  { that.changeAngle(v, that) }
        });

        $(this.dom).find('.cs_knob_shadow').change(function(e) {
        e.preventDefault();
            $('.cs_knob')
                .val($(e.target).val())
                .trigger('change');
            $("#pscolor").data('pscolor').angle = $(e.target).val();
        });
        $(this.dom).find('.opacity').change(function(e) {
           //this.dom.data('pscolor').
        });

		items.children('.gradient-item').last().css('left', $('#gradient-bar').offset().left);
		items.children('.gradient-item').last().css('background-color', hsbToHex(dom.data('pscolor').shelfColor));

		items.append(this.gradientDiv());
		items.children('.gradient-item').last().css('left', ($('#gradient-bar').width() + $('#gradient-bar').offset().left) + "px");
		items.children('.gradient-item').last().css('background-color', "#" + hsbToHex(dom.data('pscolor').color) );

        var that = this;
        this.gradientObj = this;
        // Watch for color changes
        that.dom.data('pscolor').that.onColorChange(that.setColor);

		$('#gradient-bar').click(function(e) {
			var x = e.pageX - $(this).offset().left;

			// If we try to grab the color after we add the item, we'll get a transparent color.
			var color = that.getColor(x / $('#gradient-bar').width() * 100);

			items.append(that.gradientDiv());
			var item = items.children('.gradient-item').last().get();
			$(item).css('left', x);
			$(item).css('background-color', color);
			that.picker_target = item;
			that.updateItems();
			$(item).trigger('click');
		});

		$('#showgradient').click(function() {
			$('#text').slideToggle();
			if ($(this).html() == "Show CSS") {
				$(this).html("Hide CSS");
			} else {
				$(this).html("Show CSS");
			}
		});

		this.updateItems();
		$('.gradient-item').first().trigger('click');
	};


	Gradient.prototype.changeAngle = function(v,that) {
        $(".cs_knob_shadow").val(v).trigger('change');
        that.value = v;
    };
	Gradient.prototype.updateItems = function() {
	    var that = this;
		$('.gradient-item').drags({
		    axis: 'x',
		    restrict: {
		        left: $('#gradient-bar').offset().left,
		        right: ($('#gradient-bar').offset().left + $('#gradient-bar').width())
		    }
		}).on('click',
            function(e) {
                $(this).on('mousemove touchmove', function(e,t) {
                    that.dragItem(e,t);
                });
                $(that.element).find('.location').change( function(e) {
                    $drag = $('.selected');
                    drg_w = $drag.outerWidth(),
                    pos_x = ($(e.target).val()/100)*$("#gradient-bar").width(),
                    mod = {};

                    //console.log(($(e.target).val()/100)*$("#gradient-bar").width());
                    var mod_x = $("#gradient-bar").position().left;

                    mod.left = mod_x + pos_x;
                    $('.selected').offset(mod);
                });
                $(this).trigger('mousemove');
                $(document).on('mouseup touchend', function(event) {
                    $(this).off('mousemove touchmove');
                });
            }).on('contextmenu', function(e) {
                if (that.items.children('.gradient-item').length > 2) {
                    $(this).remove();
                    that.dragItem();
                }
            });

		$('.gradient-item').click(function() {
			that.picker_target = this;
			$('.gradient-item').removeClass('selected');
			$(this).addClass('selected');
			//console.log($(this).css('background-color'));
			var color = rgbToHsb(cleanRgb($(this).css('background-color')));
			that.dom.data('pscolor').that.setNewColor(color,that.dom.get(0));
		});

		this.dragItem();
	};
	Gradient.prototype.setColor = function(color) {
	    var that = this.gradientObj;
		$(that.picker_target).css('background-color', color);
		that.gradientObj.dragItem();
	};

	Gradient.prototype.getColorList = function() {
		var colors = [];
		$(this.items).children('.gradient-item').each(function() {
			var color = [];
			pos = $(this).position();
			color.col = $(this).css('background-color');
			color.pct = Math.round( (pos.left - $('#gradient-bar').offset().left) / $('#gradient-bar').width() * 100);
			colors.push(color);
		});
        //console.log(this);

		colors.sort(this.sortColors);
		return colors;
	};

	Gradient.prototype.getColor = function(percent) {
		percent = Math.round(percent);
		var colors = this.getColorList();
        //console.log(colors);
		for (var i = 0; i < colors.length; i++) {
			var c = colors[i];
			if (c.pct > percent) {
				if (i === 0) {
					//console.log("before: " + i + " " + cleanRgb(c.col));
					return rgbToHex(cleanRgb(c.col));
				} else {
					var prev = colors[i - 1];
					//console.log(percent + "% between " + (i - 1) + " (" + prev.col + ") and " + i + " (" + c.col + ")");
					return this.interpolateColors(prev.col, c.col, prev.pct, c.pct, percent);
				}
			}
		}
		console.log("after: " + colors[colors.length - 1].col);
		return rgbToHex(colors[colors.length - 1].col);
	};

	Gradient.prototype.dragItem = function(e,t) {
	    var angle =  $("#pscolor").data('pscolor').angle;
        if(e) {
            $(this.element).find('.location').val(Math.round( ($(e.target).position().left - $('#gradient-bar').offset().left) / $('#gradient-bar').width() * 100));
        }
		var colors = this.getColorList();

		var gradient = "linear-gradient( " + angle + "deg,";
		var moz_gradient = "-moz-linear-gradient( " + angle + "deg,";
		var web_gradient = "-webkit-linear-gradient( " + angle + "deg,";
		var o_gradient = "-o-linear-gradient( " + angle + "deg,";
		var ms_gradient = "-ms-linear-gradient( " + angle + "deg,";
		var webkit_gradient = "-webkit-gradient( " + angle + "deg, ";
		var items = [];
		var webkit_items = [];
		for (var i = 0; i < colors.length; i++) {
			webkit_items.push("color-stop(" + colors[i].pct + "%" + ", " + colors[i].col + ")");
			items.push(colors[i].col + " " + colors[i].pct + "%");
		}

		gradient += items.join(", ");
		gradient += ")";

		moz_gradient += items.join(", ");
		moz_gradient += ")";

		web_gradient += items.join(", ");
		web_gradient += ")";

		o_gradient += items.join(", ");
		o_gradient += ")";

		ms_gradient += items.join(", ");
		ms_gradient += ")";

		webkit_gradient += webkit_items.join(", ");
		webkit_gradient += ")";

		$('#gradient-bar').css('background', gradient);
		$('#gradient-bar').css('background', moz_gradient);
		$('#gradient-bar').css('background', web_gradient);
		$('#gradient-bar').css('background', o_gradient);
		$('#gradient-bar').css('background', ms_gradient);
		$('#gradient-bar').css('background', webkit_gradient);

		$("#pscolor").data('pscolor').gradients = {
		1: "background:" + gradient + ";",
		2: "background:" + moz_gradient + ";",
		3: "background:" + web_gradient + ";",
		4: "background:" + o_gradient + ";",
		5: "background:" + ms_gradient + ";",
		6: "background:" + webkit_gradient + ";",
		obj: {
		    normal: gradient,
		    webkit: webkit_gradient,
		    mozilla: moz_gradient,
		    nwebkit: web_gradient,
		    opera: o_gradient,
		    ms: ms_gradient
		    }
		};
	};

	Gradient.prototype.sortColors = function(c1, c2) {
		return c1.pct - c2.pct;
	};

	Gradient.prototype.interpolateColors = function(col1, col2, lower, upper, pos) {
		var x = (pos - lower) / (upper - lower);
		console.log(x + ": " + lower + " (" + col1 + ") /" + upper + " (" + col2 + ") /" + pos);
		return rgbToHex(this.rgbObjectInterp(cleanRgb(col1), cleanRgb(col2), x));
	};

	Gradient.prototype.rgbObjectInterp = function(rgb1, rgb2, x) {
		rgb = new Array();
		rgb.r = Math.round(this.interp(rgb1.r, rgb2.r, x));
		rgb.g = Math.round(this.interp(rgb1.g, rgb2.g, x));
		rgb.b = Math.round(this.interp(rgb1.b, rgb2.b, x));
		return rgb;
	};

	Gradient.prototype.interp = function(a, b, x) {
		var r = (parseFloat(b) - parseFloat(a)) * parseFloat(x) + parseFloat(a);
		//r += a;
		//console.log(a + ", " + b + ", " + x + ": " + r + "\n" + "b-a: " + (b-a) + " *x:" + ((b-a)*x));
		return r;
	};

	Gradient.prototype.gradientDiv = function() {
		return "<div class='gradient-item'></div>";
	};

	// Plugin declaration
	$.fn[pscolor] = function(options) {
		return this.each(function() {
			if (!$.data(this, 'plugin_' + pscolor)) {
				$.data(this, 'plugin_' + pscolor,
					new PsColor(this, options));
			}
		});
	};

})(jQuery);

(function($) {
    $.fn.drags = function(opt) {

        opt = $.extend({handle:"",cursor:"move"}, opt);

        if(opt.handle === "") {
            var $el = this;
        } else {
            var $el = this.find(opt.handle);
        }

        return $el.css('cursor', opt.cursor).on("mousedown", function(e) {
            if(opt.handle === "") {
                var $drag = $(this).addClass('draggable');
            } else {
                var $drag = $(this).addClass('active-handle').parent().addClass('draggable');
            }
            var z_idx = $drag.css('z-index'),
                drg_h = $drag.outerHeight(),
                drg_w = $drag.outerWidth(),
                pos_y = $drag.offset().top + drg_h - e.pageY,
                pos_x = $drag.offset().left + drg_w - e.pageX,
                mod = {};

            $drag.css('z-index', 1000).parents().on("mousemove", function(e) {
                var mod_x = 0,
                    mod_y = 0;

                if(opt.restrict !== undefined) {
                    var res = $.extend({left:0,right: $(document).width(),top:0,bottom: $(document).height()}, opt.restrict);
                    mod_y = Math.max(res.top,Math.min(res.bottom, e.pageY));
                    mod_x = Math.max(res.left,Math.min(res.right, e.pageX));
                }
                if(opt.axis === 'x'){
                    mod.left = mod_x + pos_x - drg_w;
                } else if(opt.axis === 'y') {
                    mod.top =  mod_y + pos_y - drg_h;
                } else {
                    mod.top = mod_y  + pos_y - drg_h;
                    mod.left = mod_x + pos_x - drg_w;
                }
                $('.draggable').offset(mod).on("mouseup", function() {
                    $(this).removeClass('draggable').css('z-index', z_idx);
                });
            });
            e.preventDefault(); // disable selection
        }).on("mouseup", function() {
            if(opt.handle === "") {
                $(this).removeClass('draggable');
            } else {
                $(this).removeClass('active-handle').parent().removeClass('draggable');
            }
        });

    }
})(jQuery);


$(function() {
    var options = {
          	    onChange: function(e) {
          	        console.log(e);
          	    },
          	    onSubmit: function(e) {
          	        console.log(e);
          	    },
          	    onClose: function(e) {
          	        console.log(e);
          	    }
          	};
          	console.log(options);
	PsColorObject = $('#pscolor').pscolor(options);
});